package br.com.feirpa.meuquiz;

import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private RadioGroup radioGroup;
    private TextView txtTitulo;
    private RadioButton optA;
    private RadioButton optB;
    private RadioButton optC;
    private Button btnOk;

    String Perguntas[] = {"Primeira Pergunta?",
            "Segunda Pergunta?",
            "Terceira Pergunta?",
            "Quarta Pergunta?",
            "Quinta Pergunta?"};

    String OpcaoA[] = {"Resposta A primeira pergunta.",
            "Resposta A segunda pergunta.",
            "Resposta A terceira pergunta.",
            "Resposta A quarta pergunta.",
            "Resposta A quinta pergunta."};

    String OpcaoB[] = {"Resposta B primeira pergunta.",
            "Resposta B segunda pergunta.",
            "Resposta B terceira pergunta.",
            "Resposta B quarta pergunta.",
            "Resposta B quinta pergunta."};

    String OpcaoC[] = {"Resposta C primeira pergunta.",
            "Resposta C segunda pergunta.",
            "Resposta C terceira pergunta.",
            "Resposta C quarta pergunta.",
            "Resposta C quinta pergunta."};

    int[] listaResposta = new int[Perguntas.length];
    int listaGabarito[] = {1, 2, 3, 1, 2};
    int respostasCorretas = 0;
    int numeroPergunta = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnOk = (Button) findViewById(R.id.btnOk);
        radioGroup = (RadioGroup) findViewById(R.id.grupoRadio);
        txtTitulo = (TextView) findViewById(R.id.txtTitulo);
        optA = (RadioButton) findViewById(R.id.optA);
        optB = (RadioButton) findViewById(R.id.optB);
        optC = (RadioButton) findViewById(R.id.optC);

        btnOk.setEnabled(false);

        AtualizaPerguntas(btnOk);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.optA:
                        Log.d("s", "Opcao n1!");
                        listaResposta[numeroPergunta - 1] = 1;
                        break;
                    case R.id.optB:
                        Log.d("s", "Opcao n2!");
                        listaResposta[numeroPergunta - 1] = 2;
                        break;
                    case R.id.optC:
                        Log.d("s", "Opcao n3!");
                        listaResposta[numeroPergunta - 1] = 3;
                        break;
                }
                btnOk.setEnabled(true);
            }
        });

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AtualizaPerguntas(btnOk);
            }
        });

    }

    private void AtualizaPerguntas(View view) {
        if (numeroPergunta == Perguntas.length) {
            radioGroup.clearCheck();
            optA.setEnabled(false);
            optB.setEnabled(false);
            optC.setEnabled(false);

            ConfereResultado();
        } else {
            txtTitulo.setText(Perguntas[numeroPergunta]);

            optA.setText(OpcaoA[numeroPergunta]);
            optB.setText(OpcaoB[numeroPergunta]);
            optC.setText(OpcaoC[numeroPergunta]);

            btnOk.setEnabled(false);
            radioGroup.clearCheck();

            numeroPergunta++;
        }
    }

    private void ConfereResultado() {
        int contadorLista = 0;
        for (int numero : listaResposta) {
            System.out.println(numero);
            if (numero == listaGabarito[contadorLista]) {
                respostasCorretas++;
                System.out.println("Resposta Correta!");
            } else {
                System.out.println("Resposta Errada!!!!");
            }
            contadorLista++;
        }
        AlertaResultado();
    }

    private void AlertaResultado() {
        AlertDialog alertDialog;
        alertDialog = new AlertDialog.Builder(this).create();
        alertDialog.setTitle("Resultado!");
        alertDialog.setMessage("Voce acertou " + respostasCorretas + " questoes!");
        alertDialog.show();

        btnOk.setEnabled(false);
    }
}
